// Autor: Martin Cernek, xcerne01 AT stud.fit.vutbr.cz
// 100% zmien, moja praca

#include <fitkitlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <lcd/display.h>
#include <keyboard/keyboard.h>

int numbers[] = { 0xC0, 0xF9, 0x8C, 0x98, 0xB1, 0x92, 0x82, 0xF8, 0x80, 0x90 }; // zobrazenie jednotlivych cislis na 7-segmentovy display
int segm_digits[] = { 0x01, 0x02, 0x04, 0x08 }; // zobrazenie cifier od 1 po 4

int counter = 0;
int per_second = 0;
int print_dots = 0;

char last_ch = '\0';
int char_cnt = 0;
char input[40];
int i = 0;
int timer_counter = 0;

int rotor_index = 0;
int rotor_stopped[] = { 0, 0, 0 }; // indikovanie zastavenie rotoru

void print_user_help(void)
{
}


unsigned char decode_user_cmd(char *cmd_ucase, char *cmd)
{
    return (CMD_UNKNOWN);
}


void fpga_initialized()
{
    LCD_init();
    LCD_write_string("MAX: ");
}

// prevzate a upravene z prednasky o FITkite
int keyboard_idle(void)
{
   char ch;
   ch = key_decode(read_word_keyboard_4x4());
   if (ch != last_ch) { // osetrenie drzania klavesnice
      last_ch = ch;
      if (ch != 0) // ak bola stlacena klavesnica
      {
         if (char_cnt > 31) { // vymaz display
            LCD_clear();
            char_cnt = 0;
         }
         if (ch == '#') {   // ukonci citanie z klavesnice
            input[i] = '\0';
            return 1;
        }
         LCD_append_char(ch);
         input[i] = ch;
         i++;
         char_cnt++;
      }
   }
   return 0;
}

int check_rotor()
{
    int i;
    for (i = 0; i < 3; i++)
    {
        if (rotor_stopped[i] != 0) {
            return 0;
        }
    }
    return 1;
}

void display_number(int num)
{
    if (num == -1) {
        P4OUT = 0x00;
        P6OUT = 0xFF;
        return;
    }
    int i;
    for (i = 3; i >= 0; i--) {
        P4OUT = segm_digits[i];
        P6OUT = numbers[num%10];
        num /= 10;
        P4OUT = 0x00;
        P6OUT = 0xFF;
        if (num == 0)
           break;
    }
}

int main(void)
{
    initialize_hardware();
    keyboard_init();
    WDG_stop();
    set_led_d5(1);

    int limit;

    P6DIR |= 0xFF;
    P6OUT = 0xFF;
    P4DIR |= 0x0F;
    P4OUT = 0x0F;


    // pin 0 na porte 2 je vstupny
    P2DIR &= ~(BIT0);
    // nastavenie vyvolania prerusenia na pine 0 porte 2
    P2IE |= (BIT0);
    P2IFG &= ~(BIT0);
    P2IES &= ~(BIT0);

    // nacitanie maximalnej hodnoty z klavesnice
    char *err;
    while (1) {
        if (keyboard_idle()) {
            // konverzia na integer a osetrenie chybne zadanej hodnoty
            // v pripade chybnej hodnoty, proces sa opakuje
            limit = strtol(input, &err, 10);
            if ((*err == 0) && (limit < 10000)) {
                break;
            }
            else {
                LCD_clear();
                LCD_write_string("MAX: ");
                i = 0;
            }
        }
    }
    
    // vyvolanie prerusenia kazdu sekundu
    // prevzate a upravene z prednasky o FITkite
    CCTL0 = CCIE;
    CCR0 =  0x2000;
    TACTL = TASSEL_1 + MC_2;


    LCD_clear();    
    while(1){
        terminal_idle();
        if ((per_second*60) > limit) {
            if (timer_counter % 2 == 0)
                display_number(-1);
            else 
                display_number(per_second*60);
        }
        else {
            display_number(per_second*60);
        }
    }
}

// prerusenie na port 2 pin 0
interrupt (PORT2_VECTOR) Port_2IN(void) {
    int i = 100;    // odstranenie zakmitov
    while (i) {
        i--;
        P2IFG = 0x00;   // nulovanie, odstranenie preruseni zo stavu pending
    }
    if (P2IN & BIT0) {  // ak je spinac zopnuty, tak sa zvysi pocitadlo
        counter++;
    }
    P2IFG = 0x00;   // nulovanie
}

// prerusenie kazdu 1 sekundu
interrupt (TIMERA0_VECTOR) Timer_A (void)
{
    flip_led_d5();
    if (timer_counter == 3) {
        per_second = counter;
        counter = 0;
        rotor_index %= 3;
        rotor_stopped[rotor_index] = per_second;
        rotor_index++;
        if (check_rotor()) {
            LCD_clear();
            LCD_write_string("ROTOR stopped..");
            print_dots = 0;
        }
        else {
            print_dots %= 4;
            if (print_dots == 0) {
                LCD_clear();
            }
            else {
                LCD_append_char('.');
            }
            print_dots++;
        }
        timer_counter = 0;
    }
    timer_counter++;
    CCR0 += 0x2000;
}
